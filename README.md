# Billedgenkendelse til Nationalmuseet

Eksperimenter med machine learning på nationalmuseets billeder:

- [20191123-sort-images.ipynb](20191123-sort-images.ipynb) Hack4dk proof-of concept med automatisk kategorisering af billeder.
- [natmus.jsonl.gz](natmus.jsonl.gz) Dump af metadata.
- [20191219-fetchimages.ipynb](20191219-fetchimages.ipynb) Download nationalmuseets billeder... NB: 55GB og tager lang tid at hente.
- [kategorier.csv](kategorier.csv) Kategorier/træningssæt fra Nationalmuseet(Jacob), til machinelearning.
- [20200109-sort-images.ipynb](20200109-sort-images.ipynb) v.1 af kategorisering ud fra træningssættet, fejlrate ca. 14%, - en del billeder fejlkategoriseres som maleri/tegning.
- [kat2.txt](kat2.txt) Større træningssæt. Eksempler tilføjet ved at køre v.1 på nogle tilfældige billeder, - og derefter kigge resultatet igennem og rette fejlene.
- [20200110-sort-images.ipynb](20200110-sort-images.ipynb) v.2 af kategorisering ud fra træningssættet, fejlrate nu ca. 3%

Bemærk, koden bære præg af at være eksperimentel kode, – der bliver lavet et andet repositorie med mere poleret kode hvis det skal sættes i drift.

## Næste trin

- Bedre træning af netværket

# Diverse noter
## Kategorisering af Nationalmuseets billeder


Forskellige projekter med nationalmuseets billeder


## Billeder til crowdsourcing

Filtrering af billeder efter hvilke der kan anvendes til crowdsourcing af geodata.

Tasks:

- udtræk træningssæt med eksempler på billeder der kan bruges til crowdsourcing
    - samlinger: 
    - location.crowd.latitude exists
    - via elastic search 
- udtræk træningssæt med billeder generelt (ikke nødvendigvist egnede til crowdsourcing)
- implementer deep learning model til at kende forskel på dem

Noter

- [Beskrivelse af nationalmuseets API](https://docs.google.com/document/d/1LsFv5_iWbjoQU8oz-Y7dit5dyyeHKZR0ixvRkcrolmM/edit) (fra hack4dk)

## Høstning af billeder

Høst af billeder, – og placering hvor de let kan tilgås.

Extract images from cumulus, i.e. https://cumulus.natmus.dk/cip-doc

## Kategorisering

Tasks:

- datasæt med kategorier og eksempelbilleder til træning


Mulige kategorier:

- museumsgenstande
- arkivalier
    - registreringskort
        - hvide billedkort (*beskæring af billederne*)
            - hvide billedkort med håndskrift
        - blå registreringskort
    - tekst
        - håndskrift
            - protokolside
            - øvrigt håndskrift
        - maskinskrift
        - øvrige skrifttegn
- fotoalbum


- personer
- bygning
- landskab

## Mulig Opgaver

- CIP – høstning af image assets fra natmus
    - bruger der kan se alle billeder
- genkendelse af foto til crowdsourcing (af geolocation)
    - m. location.crowd..
    - samlinger m. locations

---

- kategorisering - afvent kategorier med eksempler
- beskæring af hvide billedkort

---

- håndskriftgenkendelse 13K sider, ca. ti personer

