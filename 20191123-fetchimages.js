const axios = require('axios');
const {promisify} = require('util');
let { exec } = require('child_process');
exec = promisify(exec);


const path = __dirname + '/img'
async function main(samling) {
  exec(`mkdir -p ${path}/${samling}`);
  for(let i = 0; i < 10000; i += 1) {
    console.log(samling, i);
    try {
      const result = (await axios.get(`http://api.natmus.dk/search/public/simple?query=type:asset%20AND%20collection:${samling}&size=1&offset=${i}`)).data.results.map(o => o.data);;
      await Promise.all(result.map(o => exec(`cd ${path}/${samling}; wget https://cumulus.natmus.dk/CIP/preview/thumbnail/${o.collection}/${o.id}`)));
    } catch(e) {
      console.log('error', samling, i);
      // pass
    }
  }
}
main('DMR')
main('DNT')
main('DO')
main('ES')
main('AS')
